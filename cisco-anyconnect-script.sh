#!/bin/sh
# Convenience script for connecting to Cisco Anyconnect VPN network using openconnect.

# Variables, adjust as needed
Server="workspace.vpn.com"
Authgroup="10NET"
Uname="username"

# Make sure to generate password file with `gpg -c <Passwordfile>`
# Must enter 2FA for this to work
# Edge case: won't work if your password contains patterns recognizable by `echo -e`
Passfile="xxx.auth.gpg"
Passwd=$(gpg -d $Passfile)
read -p "Enter 2FA 6-digit code: " Twofa
Pass=$Passwd"\n"$Twofa

# Main script
echo "The below step must be run as root, enter root password if prompted."
echo -e "$Pass" |\
  sudo openconnect \
  --protocol=anyconnect \
  --server="$Server" \
  --user="$Uname" \
  --authgroup="$Authgroup" \
  --passwd-on-stdin